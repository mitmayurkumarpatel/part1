package Airplane;

/**
 *
 * @author mitpa
 */
public class Airport 
{
    private String Code;
    private String City;

    public Airport(String Code, String City) {
        this.Code = Code;
        this.City = City;
    }

    public String getCode() {
        return Code;
    }

    public String getCity() {
        return City;
    }
    
    
}
