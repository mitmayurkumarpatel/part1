
package Airplane;

import java.util.Date;

/**
 *
 * @author mitpa
 */
public class Flight 
{
    private int number;
    private Date date;

    public Flight(int number, Date date) {
        this.number = number;
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }
    
    
    
}
