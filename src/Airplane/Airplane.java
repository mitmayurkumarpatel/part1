
package Airplane;

/**
 *
 * @author mitpa
 */
public class Airplane 
{
    private int Id;
    private int numberOfSeats;
    private String make;
    private String model;

     public Airplane(int Id, int numberOfSeats, String make, String model) {
        this.Id = Id;
        this.numberOfSeats = numberOfSeats;
        this.make = make;
        this.model = model;
    }
     
    public int getId() {
        return Id;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

   
    
}
